package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText et_username, et_password;
    Button b_resetPassword, b_register, b_shopWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_username = findViewById(R.id.ET_main_username);
        et_password = findViewById(R.id.ET_main_password);
        b_resetPassword = findViewById(R.id.B_main_resetPassword);
        b_register = findViewById(R.id.B_main_register);
        b_shopWindow = findViewById(R.id.B_main_shopWindow);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        et_username.setText(thisIntent.getStringExtra("username"));

        // performs login if user is already logged in the application
        // username = loginData[0], password = loginData[1]
        String[] loginData = AppPreferences.getLoginData(this);
        et_username.setText(loginData[0]);
        et_password.setText(loginData[1]);

        if (loginData[0].length() > 0 && loginData[1].length() > 0) {
            ServiceCaller serviceCaller = new ServiceCaller(this, getWindow().getDecorView().getRootView(),
                    "Login", loginData[0], loginData[1]);
            serviceCaller.execute("get", "true");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_shopWindow:
                b_shopWindow.performClick();
                return true;
            case R.id.action_resetPassword:
                b_resetPassword.performClick();
                return true;
            case R.id.action_register:
                b_register.performClick();
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void resetColors() {
        et_username.getBackground().clearColorFilter();
        et_password.getBackground().clearColorFilter();
    }

    private boolean checkInput() {
        resetColors();
        boolean inputsOk = true;

        if (et_username.getText().length() == 0) {
            et_username.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        if (et_password.getText().length() == 0) {
            et_password.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        return inputsOk;
    }

    public void B_main_login_onClick(View view) {
        if (checkInput()) {
            ServiceCaller serviceCaller = new ServiceCaller(this, view, "Login",
                    et_username.getText().toString(), et_password.getText().toString());
            serviceCaller.execute("get", "true");
        }
    }

    public void B_main_resetPassword_onClick(View view) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        intent.putExtra("username", et_username.getText().toString());

        startActivity(intent);
    }

    public void B_main_register_onClick(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        intent.putExtra("username", et_username.getText().toString());

        startActivity(intent);
    }

    public void B_main_shopWindow_onClick(View view) {
        Intent intent = new Intent(this, ShopWindowActivity.class);

        startActivity(intent);
    }
}

package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class ResetPasswordActivity extends AppCompatActivity {
    EditText et_username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        et_username = findViewById(R.id.ET_resetPassword_username);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        et_username.setText(thisIntent.getStringExtra("username"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reset_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_shopWindow:
                intent = new Intent(this, ShopWindowActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_register:
                intent = new Intent(this, RegisterActivity.class);
                intent.putExtra("username", et_username.getText().toString());
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void resetColors() {
        et_username.getBackground().clearColorFilter();
    }

    private boolean checkInput() {
        resetColors();
        boolean inputsOk = true;

        if (et_username.getText().length() == 0) {
            et_username.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        return inputsOk;
    }

    public void B_resetPassword_reset_onClick(View view) {
        if (checkInput()) {
            ServiceCaller serviceCaller = new ServiceCaller(this, view, "ResetPassword", et_username.getText().toString());
            serviceCaller.setBody(et_username.getText().toString());
            serviceCaller.execute("post", "false", "true");
        }
    }
}

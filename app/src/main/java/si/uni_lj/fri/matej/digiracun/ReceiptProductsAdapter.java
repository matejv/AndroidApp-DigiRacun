package si.uni_lj.fri.matej.digiracun;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static si.uni_lj.fri.matej.digiracun.BaseClass.toDateDisplay;

public class ReceiptProductsAdapter extends RecyclerView.Adapter<ReceiptProductsAdapter.ViewHolder> {
    private List<JSONObject> receiptProducts;
    private LayoutInflater inflater;

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tv_productName, tv_price, tv_quantity, tv_productSerialNo, tv_dateOfGuarantee, tv_sumPrice;

        public ViewHolder(View view) {
            super(view);
            tv_productName = view.findViewById(R.id.TV_receiptProductItem_productName);
            tv_price = view.findViewById(R.id.TV_receiptProductItem_price_val);
            tv_quantity = view.findViewById(R.id.TV_receiptProductItem_quantity_val);
            tv_productSerialNo = view.findViewById(R.id.TV_receiptProductItem_productSerialNo_val);
            tv_dateOfGuarantee = view.findViewById(R.id.TV_receiptProductItem_dateOfGuarantee_val);
            tv_sumPrice = view.findViewById(R.id.TV_receiptProductItem_sumPrice_val);
        }
    }

    public ReceiptProductsAdapter(Context context, List<JSONObject> receiptProducts) {
        this.inflater = LayoutInflater.from(context);
        this.receiptProducts = receiptProducts;
    }

    // inflates the row layout from xml when needed
    @Override
    public ReceiptProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = inflater.inflate(R.layout.item_receipt_product, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JSONObject product = receiptProducts.get(position);
        double price = 0.0;
        int quantity = 1;

        try {
            price = product.getDouble("price");
            quantity = product.getInt("quantity");

            holder.tv_productName.setText(product.getString("productName"));
            holder.tv_price.setText(String.format("%.2f €", price));
            holder.tv_quantity.setText(Integer.toString(quantity));
            holder.tv_productSerialNo.setText(product.getString("serialNo"));
            holder.tv_dateOfGuarantee.setText(toDateDisplay(product.getString("dateOfGuarantee")));
            holder.tv_sumPrice.setText(String.format("%.2f €", price * quantity));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return receiptProducts.size();
    }
}

package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static si.uni_lj.fri.matej.digiracun.BaseClass.deleteCache;

public class UserActivity extends AppCompatActivity implements UserReceiptsAdapter.ItemClickListener {
    private static String username, password;
    public static ArrayList<JSONObject> receipts;
    static UserReceiptsAdapter adapter;
    RecyclerView rv_receipts;
    FloatingActionButton fab_addReceipt, fab_addReceipt_picture, fab_addReceipt_qrcode, fab_addReceipt_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        fab_addReceipt = findViewById(R.id.FAB_user_addReceipt);
        fab_addReceipt_picture = findViewById(R.id.FAB_user_addReceipt_picture);
        fab_addReceipt_qrcode = findViewById(R.id.FAB_user_addReceipt_qrcode);
        fab_addReceipt_code = findViewById(R.id.FAB_user_addReceipt_code);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");
        receipts = new ArrayList<JSONObject>();

        ServiceCaller serviceCaller = new ServiceCaller(this, "GetReceipts", username, password);
        serviceCaller.execute("get", "true");

        // wait for a list receipts from web service up to 500 milliseconds
        try {
            serviceCaller.get(500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        rv_receipts = findViewById(R.id.RV_user_receipts);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_receipts.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_receipts.getContext(), layoutManager.getOrientation());
        rv_receipts.addItemDecoration(dividerItemDecoration);
        adapter = new UserReceiptsAdapter(this, receipts);
        adapter.setClickListener(this);
        rv_receipts.setAdapter(adapter);
    }

    @Override
    public void onRestart() {
        super.onRestart();

        ServiceCaller serviceCaller = new ServiceCaller(this, "GetReceipts", username, password);
        serviceCaller.execute("get", "true");
        adapter.notifyDataSetChanged();

        finish();
        startActivity(getIntent());
    }

    @Override
    protected void onDestroy() {
        deleteCache(getApplicationContext());

        super.onDestroy();
    }

    public static void refreshData() {
        Collections.sort(receipts, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
                Date d1, d2;

                try {
                    d1 = format.parse(o1.getString("dateOfPurchase"));
                    d2 = format.parse(o2.getString("dateOfPurchase"));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }

                if (d1.getTime() == d2.getTime()) {
                    try {
                        String rn1 = o1.getString("receiptNumber");
                        String rn2 = o2.getString("receiptNumber");

                        return rn1.compareToIgnoreCase(rn2) * -1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return d1.after(d2) ? -1 : 1;
            }
        });

        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_userMaterials:
                intent = new Intent(this, UserMaterialsActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_shopWindow:
                intent = new Intent(this, ShopWindowActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_userProfile:
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(this, EditReceiptActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        try {
            JSONObject jsonObject = adapter.getItem(position);
            intent.putExtra("receiptId", jsonObject.getString("id"));
            intent.putExtra("code", jsonObject.getString("code"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        startActivity(intent);
    }

    public void FAB_user_addReceipt_onClick(View view) {
        if (fab_addReceipt_picture.getVisibility() == View.VISIBLE) {
            fab_addReceipt_picture.setVisibility(View.GONE);
            fab_addReceipt_qrcode.setVisibility(View.GONE);
            fab_addReceipt_code.setVisibility(View.GONE);
            fab_addReceipt.setImageResource(R.drawable.ic_action_add);
        } else {
            fab_addReceipt_picture.setVisibility(View.VISIBLE);
            fab_addReceipt_qrcode.setVisibility(View.VISIBLE);
            fab_addReceipt_code.setVisibility(View.VISIBLE);
            fab_addReceipt.setImageResource(R.drawable.ic_action_add_close);
        }
    }

    public void FAB_user_addReceipt_picture_onClick(View view) {
        Intent intent = new Intent(this, AddReceiptPictureActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);

        startActivity(intent);
    }

    public void FAB_user_addReceipt_qrcode_onClick(View view) {
        Intent intent = new Intent(this, AddReceiptCodeActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        intent.putExtra("qrScanner", true);

        startActivity(intent);
    }

    public void FAB_user_addReceipt_code_onClick(View view) {
        Intent intent = new Intent(this, AddReceiptCodeActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);

        startActivity(intent);
    }
}

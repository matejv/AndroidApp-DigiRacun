package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class AddReceiptCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private static String username, password;
    EditText et_receiptCode;
    private boolean qrScanner;
    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receipt_code);

        et_receiptCode = findViewById(R.id.ET_addReceiptCode_receiptCode);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");
        et_receiptCode.setText(thisIntent.getStringExtra("receiptCode"));
        qrScanner = thisIntent.getBooleanExtra("qrScanner", false);

        if (qrScanner) {
            mScannerView = new ZXingScannerView(this); // Programmatically initialize the scanner view
            setContentView(mScannerView); // Set the scanner view as the content view
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (qrScanner) {
            mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView.startCamera(); // Start camera on resume
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (qrScanner) {
            mScannerView.stopCamera(); // Stop camera on pause
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_receipt_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_userMaterials:
                intent = new Intent(this, UserMaterialsActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_shopWindow:
                intent = new Intent(this, ShopWindowActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_userProfile:
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleResult(Result rawResult) {
        Intent intent = getIntent();
        intent.putExtra("qrScanner", false);
        intent.putExtra("receiptCode", rawResult.getText());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);
    }

    private void resetColors() {
        et_receiptCode.getBackground().clearColorFilter();
    }

    private boolean checkInput() {
        resetColors();
        boolean inputsOk = true;

        if (et_receiptCode.getText().length() == 0) {
            et_receiptCode.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        return inputsOk;
    }

    public void B_addReceiptCode_addByCode_onClick(View view) {
        if (checkInput()) {
            ServiceCaller serviceCaller = new ServiceCaller(this, view, "AddReceiptCode", username, password);
            serviceCaller.setBody(et_receiptCode.getText().toString());
            serviceCaller.execute("post", "true", "true");
        }
    }
}

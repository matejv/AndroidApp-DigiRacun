package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ShopWindowActivity extends AppCompatActivity implements ShopProductsAdapter.ItemClickListener {
    private static String username, password;
    public static ArrayList<JSONObject> shopProducts;
    static ShopProductsAdapter adapter;
    RecyclerView rv_products;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_window);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");
        shopProducts = new ArrayList<JSONObject>();

        ServiceCaller serviceCaller = new ServiceCaller(this, "GetProductsShopWindow");
        serviceCaller.execute("get", "false");

        // wait for a list shopProducts from web service up to 500 milliseconds
        try {
            serviceCaller.get(500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        rv_products = findViewById(R.id.RV_shopWindow_products);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        rv_products.setLayoutManager(layoutManager);
        adapter = new ShopProductsAdapter(this, shopProducts);
        adapter.setClickListener(this);
        rv_products.setAdapter(adapter);
    }

    public static void refreshData() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (username != null && password != null) {
            getMenuInflater().inflate(R.menu.menu_shop_window_logged, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_shop_window, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_login:
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_home:
                intent = new Intent(this, UserActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_userProfile:
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(this, ShopWindowProductActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        intent.putExtra("productId", adapter.getItem(position));

        startActivity(intent);
    }
}

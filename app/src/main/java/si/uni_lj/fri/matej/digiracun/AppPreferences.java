package si.uni_lj.fri.matej.digiracun;

import android.content.Context;
import android.preference.PreferenceManager;

import static android.content.SharedPreferences.Editor;

public class AppPreferences {
    private static final String PREF_USERNAME = "username";
    private static final String PREF_PASSWORD = "password";

    public static void setLoginData(Context context, String username, String password) {
        Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(PREF_USERNAME, username);
        editor.putString(PREF_PASSWORD, password);
        editor.apply();
    }

    public static String[] getLoginData(Context context) {
        String[] loginData = new String[2];
        loginData[0] = PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_USERNAME, "");
        loginData[1] = PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_PASSWORD, "");
        return loginData;
    }

    public static void clearLoginData(Context context) {
        Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.remove(PREF_USERNAME);
        editor.remove(PREF_PASSWORD);
        editor.apply();
    }
}

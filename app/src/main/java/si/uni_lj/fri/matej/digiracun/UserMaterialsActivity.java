package si.uni_lj.fri.matej.digiracun;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class UserMaterialsActivity extends AppCompatActivity implements UserMaterialsAdapter.ItemClickListener {
    private static String username, password;
    private final String TASK_MATERIALS = "GetUserMaterials/?q=%s";
    private static String SORT; // which sort order is selected
    public static ArrayList<JSONObject> productCategories;
    private static String[][] categories; // categories[0] = id, categories[1] = category
    private static boolean[] checkedCategories;
    public static ArrayList<JSONObject> userMaterials;
    static UserMaterialsAdapter adapter;
    RecyclerView rv_materials;
    Spinner s_sort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_materials);

        s_sort = findViewById(R.id.S_userMaterials_sort);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");
        SORT = getResources().getStringArray(R.array.S_userMaterials_sort_values)[s_sort.getSelectedItemPosition()];
        productCategories = new ArrayList<JSONObject>();
        userMaterials = new ArrayList<JSONObject>();

        ServiceCaller getProductCategories = new ServiceCaller(this, "GetProductCategories");
        getProductCategories.execute("get", "false");

        ServiceCaller getUserMaterials = new ServiceCaller(this, getWindow().getDecorView().getRootView(),
                String.format(TASK_MATERIALS, "all"), username, password);
        getUserMaterials.execute("get", "true");

        // wait for lists productCategories and userMaterials from web service up to 500 milliseconds
        try {
            getProductCategories.get(500, TimeUnit.MILLISECONDS);
            getUserMaterials.get(500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        rv_materials = findViewById(R.id.RV_userMaterials_materials);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_materials.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_materials.getContext(), layoutManager.getOrientation());
        rv_materials.addItemDecoration(dividerItemDecoration);
        adapter = new UserMaterialsAdapter(this, userMaterials);
        adapter.setClickListener(this);
        rv_materials.setAdapter(adapter);

        s_sort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SORT = getResources().getStringArray(R.array.S_userMaterials_sort_values)[position];
                refreshData(getWindow().getDecorView().getRootView());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_materials, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_shopWindow:
                intent = new Intent(this, ShopWindowActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_userProfile:
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void setFilters() {
        categories = new String[2][productCategories.size()];
        checkedCategories = new boolean[productCategories.size()];

        try {
            for (int i = 0; i < productCategories.size(); i++) {
                categories[0][i] = productCategories.get(i).getString("id");
                categories[1][i] = productCategories.get(i).getString("category");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static void sortDefault() {
        Collections.sort(userMaterials, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
                Date d1, d2;

                try {
                    d1 = format.parse(o1.getString("dateOfPurchase"));
                    d2 = format.parse(o2.getString("dateOfPurchase"));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }

                if (d1.getTime() == d2.getTime()) {
                    try {
                        String rn1 = o1.getString("receiptNumber");
                        String rn2 = o2.getString("receiptNumber");

                        if (rn1.equals(rn2)) {
                            String pn1 = o1.getString("productName");
                            String pn2 = o2.getString("productName");

                            return pn1.compareToIgnoreCase(pn2);
                        }

                        return rn1.compareToIgnoreCase(rn2) * -1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return d1.after(d2) ? -1 : 1;
            }
        });
    }

    private static void sortByProduct(final int direction) {
        Collections.sort(userMaterials, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                try {
                    String pn1 = o1.getString("productName");
                    String pn2 = o2.getString("productName");
                    return pn1.compareToIgnoreCase(pn2) * direction;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
    }

    private static void sortByShop(final int direction) {
        Collections.sort(userMaterials, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                try {
                    String pn1 = o1.getString("shop");
                    String pn2 = o2.getString("shop");
                    return pn1.compareToIgnoreCase(pn2) * direction;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
    }

    private static void sortByReceiptNo(final int direction) {
        Collections.sort(userMaterials, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                try {
                    String pn1 = o1.getString("receiptNumber");
                    String pn2 = o2.getString("receiptNumber");
                    return pn1.compareToIgnoreCase(pn2) * direction;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
    }

    private static void sortByDate(final int direction) {
        Collections.sort(userMaterials, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");

                try {
                    Date d1 = format.parse(o1.getString("dateOfPurchase"));
                    Date d2 = format.parse(o2.getString("dateOfPurchase"));
                    return (d1.after(d2) ? -1 : 1) * direction;
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
    }

    private static void sortByMaterial(final int direction) {
        Collections.sort(userMaterials, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                try {
                    boolean m1 = o1.getBoolean("picture");
                    boolean m2 = o2.getBoolean("picture");

                    if (m1 == m2) return 0; // the same file types
                    return (m1 ? -1 : 1) * direction; // o1 is picture
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
    }

    public static void refreshData(View view) {
        switch (SORT) {
            case "productAsc":
                sortByProduct(1);
                break;
            case "productDesc":
                sortByProduct(-1);
                break;
            case "shopAsc":
                sortByShop(1);
                break;
            case "shopDesc":
                sortByShop(-1);
                break;
            case "receiptNoAsc":
                sortByReceiptNo(1);
                break;
            case "receiptNoDesc":
                sortByReceiptNo(-1);
                break;
            case "dateAsc":
                sortByDate(1);
                break;
            case "dateDesc":
                sortByDate(-1);
                break;
            case "materialPic":
                sortByMaterial(1);
                break;
            case "materialPdf":
                sortByMaterial(-1);
                break;
            case "default":
            default:
                sortDefault();
                break;
        }

        adapter.notifyDataSetChanged();
        ((RecyclerView) view.findViewById(R.id.RV_userMaterials_materials)).smoothScrollToPosition(0);
    }

    @Override
    public void onItemClick(View view, int position) {
        JSONObject jsonObject = adapter.getItem(position);

        try {
            byte[] decoded = Base64.decode(jsonObject.getString("material"), Base64.DEFAULT);
            String fileName = jsonObject.getString("fileName");

            if (jsonObject.getBoolean("picture")) {
                if (fileName.length() == 0) {
                    fileName = "tempMaterialPicture.jpg";
                }

                File outputDir = new File(view.getContext().getExternalCacheDir(), fileName); // context being the Activity pointer
                FileOutputStream tempFile = new FileOutputStream(outputDir.getAbsoluteFile());

                final Bitmap picture = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
                picture.compress(Bitmap.CompressFormat.JPEG, 100, tempFile);
                tempFile.flush();
                tempFile.close();

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setDataAndType(Uri.parse("file:" + outputDir.getAbsolutePath()), "image/*");
                startActivity(intent);
            } else {
                if (fileName.length() == 0) {
                    fileName = "tempMaterialFile.pdf";
                }

                File outputDir = new File(view.getContext().getExternalCacheDir(), fileName); // context being the Activity pointer
                FileOutputStream tempFile = new FileOutputStream(outputDir.getAbsoluteFile());

                tempFile.write(decoded);
                tempFile.flush();
                tempFile.close();

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setDataAndType(Uri.parse("file:" + outputDir.getAbsolutePath()), "application/pdf");
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void B_userMaterials_filter_onClick(View view) {
        final Activity thisActivity = this;
        final boolean[] tempCheckedCategories = Arrays.copyOf(checkedCategories, checkedCategories.length);

        AlertDialog.Builder filterDialog = new AlertDialog.Builder(this);
        filterDialog.setTitle(R.string.userMaterials_setFilter_title);
        filterDialog.setMultiChoiceItems(categories[1], tempCheckedCategories, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                tempCheckedCategories[which] = isChecked;
            }
        });
        filterDialog.setNegativeButton(R.string.userMaterials_setFilter_negative, null);
        filterDialog.setPositiveButton(R.string.userMaterials_setFilter_positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkedCategories = Arrays.copyOf(tempCheckedCategories, checkedCategories.length);

                StringBuilder selectedCategories = new StringBuilder();

                for (int i = 0; i < checkedCategories.length; i++) {
                    if (checkedCategories[i]) {
                        if (selectedCategories.length() > 0) selectedCategories.append("$");
                        selectedCategories.append(categories[0][i]);
                    }
                }

                ServiceCaller getUserMaterials;

                if (selectedCategories.length() > 0) {
                    getUserMaterials = new ServiceCaller(thisActivity, getWindow().getDecorView().getRootView(),
                            String.format(TASK_MATERIALS, selectedCategories.toString()), username, password);
                } else {
                    getUserMaterials = new ServiceCaller(thisActivity, getWindow().getDecorView().getRootView(),
                            String.format(TASK_MATERIALS, "all"), username, password);
                }

                getUserMaterials.execute("get", "true");
            }
        });

        filterDialog.create().show();
    }

    public void B_userMaterials_removeFilter_onClick(View view) {
        checkedCategories = new boolean[checkedCategories.length];

        ServiceCaller getUserMaterials = new ServiceCaller(this, getWindow().getDecorView().getRootView(),
                String.format(TASK_MATERIALS, "all"), username, password);
        getUserMaterials.execute("get", "true");
    }
}

package si.uni_lj.fri.matej.digiracun;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static si.uni_lj.fri.matej.digiracun.BaseClass.toDateDisplay;

public class ServiceCaller extends AsyncTask<String, Void, String> {
    private final String SERVICE_URL = "https://digiracun.azurewebsites.net/Service1.svc/%s";
    private Activity activity;
    private View view;
    private String task;
    private String username;
    private String password;
    private String auth; // authorization in format: username:password
    private byte[] body; // body of POST request

    public ServiceCaller(Activity activity, String task) {
        this.activity = activity;
        this.task = task;
    }

    public ServiceCaller(Activity activity, View view, String task, String username) {
        this(activity, task);
        this.view = view;
        this.username = username;
    }

    public ServiceCaller(Activity activity, String task, String username, String password) {
        this(activity, task);
        this.username = username;
        this.password = password;
        this.auth = String.format("%s:%s", username, password);
    }

    public ServiceCaller(Activity activity, View view, String task, String username, String password) {
        this(activity, task, username, password);
        this.view = view;
    }

    public void setBody(String body) {
        try {
            this.body = body.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private String methodGet(String callURL, boolean authorization) {
        StringBuilder result = new StringBuilder();

        try {
            URL url = new URL(callURL);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod("GET");

            if (authorization)
                connection.setRequestProperty("Authorization", auth);

            connection.connect();
            int status = connection.getResponseCode();

            if (status >= HttpsURLConnection.HTTP_BAD_REQUEST) {
                connection.disconnect();
                return Integer.toString(status);
            }

            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            reader.close();
            inputStream.close();
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    private String methodPost(String callURL, boolean authorization, boolean setBody) {
        StringBuilder result = new StringBuilder();

        try {
            URL url = new URL(callURL);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod("POST");

            if (authorization)
                connection.setRequestProperty("Authorization", auth);

            if (setBody) {
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(body);
                outputStream.close();
            }

            connection.connect();
            int status = connection.getResponseCode();

            if (status >= HttpsURLConnection.HTTP_BAD_REQUEST) {
                connection.disconnect();
                return Integer.toString(status);
            }

            connection.disconnect();
            result.append(status);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    @Override
    protected String doInBackground(String... params) {
        String method = "", callURL;
        boolean authorization = false, setBody = false;

        // GET or POST method
        if (params.length > 0)
            method = params[0];

        // if authorization required
        if (params.length > 1 && params[1].equals("true"))
            authorization = true;

        // if body is required
        if (params.length > 2 && params[2].equals("true"))
            setBody = true;

        callURL = String.format(SERVICE_URL, task);

        if (method.equals("get"))
            return methodGet(callURL, authorization);

        if (method.equals("post"))
            return methodPost(callURL, authorization, setBody);

        return "";
    }

    private void userActivity() {
        Intent intent = new Intent(activity, UserActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        view.getContext().startActivity(intent);
    }

    private void loginActivity(boolean result) {
        if (result) {
            AppPreferences.setLoginData(activity.getApplicationContext(), username, password);
            userActivity();
        } else {
            Toast.makeText(activity, R.string.ERR_login, Toast.LENGTH_SHORT).show();
        }
    }

    private void resetPasswordActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_resetPassword, Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(activity, MainActivity.class);
            intent.putExtra("username", username);

            view.getContext().startActivity(intent);
        } else {
            Toast.makeText(activity, R.string.ERR_resetPassword, Toast.LENGTH_SHORT).show();
        }
    }

    private void registerActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_register, Toast.LENGTH_SHORT).show();
            AppPreferences.setLoginData(activity.getApplicationContext(), username, password);
            userActivity();
        } else if (result == HttpsURLConnection.HTTP_CONFLICT) {
            Toast.makeText(activity, R.string.ERR_register, Toast.LENGTH_SHORT).show();
        }
    }

    private void getUserProfileActivity(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            ((EditText) activity.findViewById(R.id.ET_userProfile_username)).setText(jsonObject.getString("username"));
            ((EditText) activity.findViewById(R.id.ET_userProfile_name)).setText(jsonObject.getString("name"));
            ((EditText) activity.findViewById(R.id.ET_userProfile_surname)).setText(jsonObject.getString("surname"));
            ((EditText) activity.findViewById(R.id.ET_userProfile_email)).setText(jsonObject.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void editUserActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_userProfile_changeProfile, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, R.string.ERR_userProfile_changeProfile, Toast.LENGTH_SHORT).show();
        }
    }

    private void changeUserPasswordActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_userProfile_changePassword, Toast.LENGTH_SHORT).show();
        } else if (result == HttpsURLConnection.HTTP_UNAUTHORIZED) {
            activity.findViewById(R.id.ET_userProfile_passwordNow).getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            Toast.makeText(activity, R.string.ERR_userProfile_changePassword_passwordNow, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, R.string.ERR_userProfile_changePassword, Toast.LENGTH_SHORT).show();
        }
    }

    private void getReceiptsActivity(String result) {
        try {
            JSONArray jsonArray = new JSONArray(result);
            UserActivity.receipts.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                UserActivity.receipts.add(jsonArray.getJSONObject(i));
            }

            UserActivity.refreshData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getReceiptActivity(String result) {
        try {
            final JSONObject jsonObject = new JSONObject(result);

            ((EditText) activity.findViewById(R.id.ET_editReceipt_shop)).setText(jsonObject.getString("shop"));
            ((EditText) activity.findViewById(R.id.ET_editReceipt_receiptNumber)).setText(jsonObject.getString("receiptNumber"));
            ((EditText) activity.findViewById(R.id.ET_editReceipt_dateOfPurchase)).setText(toDateDisplay(jsonObject.getString("dateOfPurchase")));

            // receipt with picture
            if (jsonObject.getString("code").length() == 0) {
                ((EditText) activity.findViewById(R.id.ET_editReceipt_dateOfGuarantee)).setText(toDateDisplay(jsonObject.getString("dateOfGuarantee")));

                byte[] decoded = Base64.decode(jsonObject.getString("receiptPicture"), Base64.DEFAULT);
                final Bitmap picture = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
                ImageView iv_receiptPicture = activity.findViewById(R.id.IV_editReceipt_receiptPicture);
                iv_receiptPicture.setImageBitmap(picture);

                activity.findViewById(R.id.B_editReceipt_fullPicture).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String receiptPicFileName = jsonObject.getString("receiptPicFileName");

                            if (receiptPicFileName.length() == 0) {
                                receiptPicFileName = "tempReceiptPicture.jpg";
                            }

                            File outputDir = new File(view.getContext().getExternalCacheDir(), receiptPicFileName); // context being the Activity pointer
                            FileOutputStream tempPicture = new FileOutputStream(outputDir.getAbsoluteFile());
                            picture.compress(Bitmap.CompressFormat.JPEG, 100, tempPicture);
                            tempPicture.flush();
                            tempPicture.close();

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.setDataAndType(Uri.parse("file:" + outputDir.getAbsolutePath()), "image/*");

                            activity.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateReceiptActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_editReceipt_updateReceipt, Toast.LENGTH_SHORT).show();
        } else if (result == HttpsURLConnection.HTTP_UNAUTHORIZED) {
            Toast.makeText(activity, R.string.ERR_editReceipt_updateReceipt_unauthorized, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, R.string.ERR_editReceipt_updateReceipt, Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteReceiptActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_editReceipt_deleteReceipt, Toast.LENGTH_SHORT).show();
            userActivity();
        } else if (result == HttpsURLConnection.HTTP_UNAUTHORIZED) {
            Toast.makeText(activity, R.string.ERR_editReceipt_deleteReceipt_unauthorized, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, R.string.ERR_editReceipt_deleteReceipt, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("DefaultLocale")
    private void getProductsOnReceiptActivity(String result) {
        double sumPriceAll = 0.0;

        try {
            JSONArray jsonArray = new JSONArray(result);
            EditReceiptActivity.EditReceiptFragment2.receiptProducts.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                EditReceiptActivity.EditReceiptFragment2.receiptProducts.add(jsonObject);
                sumPriceAll += jsonObject.getDouble("price") * jsonObject.getInt("quantity");
            }

            ((TextView) activity.findViewById(R.id.TV_editReceipt_sumPriceAll_val)).setText(String.format("%.2f €", sumPriceAll));

            EditReceiptActivity.EditReceiptFragment2.refreshData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getMaterialsOnReceiptActivity(String result) {
        try {
            JSONArray jsonArray = new JSONArray(result);
            EditReceiptActivity.EditReceiptFragment3.receiptMaterials.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                EditReceiptActivity.EditReceiptFragment3.receiptMaterials.add(jsonArray.getJSONObject(i));
            }

            EditReceiptActivity.EditReceiptFragment3.refreshData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addReceiptPictureActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_addReceiptPicture_addByPicture, Toast.LENGTH_SHORT).show();
            userActivity();
        } else if (result == HttpsURLConnection.HTTP_UNAUTHORIZED) {
            Toast.makeText(activity, R.string.ERR_addReceiptPicture_addByPicture_unauthorized, Toast.LENGTH_SHORT).show();
        } else if (result == HttpsURLConnection.HTTP_UNSUPPORTED_TYPE) {
            Toast.makeText(activity, R.string.ERR_addReceiptPicture_addByPicture_unsupportedType, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, R.string.ERR_addReceiptPicture_addByPicture, Toast.LENGTH_SHORT).show();
        }
    }

    private void addReceiptCodeActivity(int result) {
        if (result == HttpsURLConnection.HTTP_OK) {
            Toast.makeText(activity, R.string.OK_addReceiptCode_addByCode, Toast.LENGTH_SHORT).show();
            userActivity();
        } else if (result == HttpsURLConnection.HTTP_UNAUTHORIZED) {
            Toast.makeText(activity, R.string.ERR_addReceiptCode_addByCode_unauthorized, Toast.LENGTH_SHORT).show();
        } else if (result == HttpsURLConnection.HTTP_NOT_FOUND) {
            Toast.makeText(activity, R.string.ERR_addReceiptCode_addByCode_notFound, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, R.string.ERR_addReceiptCode_addByCode, Toast.LENGTH_SHORT).show();
        }
    }

    private void getProductCategoriesActivity(String result) {
        try {
            JSONArray jsonArray = new JSONArray(result);
            UserMaterialsActivity.productCategories.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                UserMaterialsActivity.productCategories.add(jsonArray.getJSONObject(i));
            }

            UserMaterialsActivity.setFilters();

            activity.findViewById(R.id.B_userMaterials_filter).setEnabled(true);
            activity.findViewById(R.id.B_userMaterials_removeFilter).setEnabled(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getUserMaterialsActivity(String result) {
        try {
            JSONArray jsonArray = new JSONArray(result);
            UserMaterialsActivity.userMaterials.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                UserMaterialsActivity.userMaterials.add(jsonArray.getJSONObject(i));
            }

            UserMaterialsActivity.refreshData(view);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getProductsShopWindowActivity(String result) {
        try {
            JSONArray jsonArray = new JSONArray(result);
            ShopWindowActivity.shopProducts.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                ShopWindowActivity.shopProducts.add(jsonArray.getJSONObject(i));
            }

            ShopWindowActivity.refreshData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("DefaultLocale")
    private void getProductShopWindowActivity(String result) {
        try {
            final JSONObject jsonObject = new JSONObject(result);
            String description = jsonObject.getString("description").replace("\\n", "\n");
            String techDetails = jsonObject.getString("techDetails").replace("\\n", "\n");
            String price = String.format("%.2f €", jsonObject.getDouble("price"));

            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_productName)).setText(jsonObject.getString("productName"));
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_category)).setText(jsonObject.getString("category"));
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_description)).setText(description);
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_price_val)).setText(price);
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_techDetails_val)).setText(techDetails);
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_shopName_val)).setText(jsonObject.getString("shopName"));
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_address_val)).setText(jsonObject.getString("shopAddress"));
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_webpage_val)).setText(jsonObject.getString("shopWebpage"));
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_phone_val)).setText(jsonObject.getString("shopPhone"));
            ((TextView) activity.findViewById(R.id.TV_shopWindowProduct_email_val)).setText(jsonObject.getString("shopEmail"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getProductPicturesShopWindowActivity(String result) {
        try {
            JSONArray jsonArray = new JSONArray(result);
            ShopWindowProductActivity.ShopWindowProductFragment0.shopProductPictures.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                byte[] decoded = Base64.decode(jsonObject.getString("material"), Base64.DEFAULT);
                final Bitmap picture = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);

                ShopWindowProductActivity.ShopWindowProductFragment0.shopProductPictures.add(picture);
            }

            if (ShopWindowProductActivity.ShopWindowProductFragment0.shopProductPictures.size() > 0) {
                ((ImageView) activity.findViewById(R.id.IV_shopWindowProduct_pictures)).setImageBitmap(
                        ShopWindowProductActivity.ShopWindowProductFragment0.shopProductPictures.get(0));
            }

            if (ShopWindowProductActivity.ShopWindowProductFragment0.shopProductPictures.size() > 1) {
                activity.findViewById(R.id.IB_shopWindowProduct_next).setEnabled(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        String taskNoParameters = task.split("/")[0];

        switch (taskNoParameters) {
            case "Login":
                loginActivity(Boolean.parseBoolean(result));
                break;
            case "ResetPassword":
                resetPasswordActivity(Integer.parseInt(result));
                break;
            case "Register":
                registerActivity(Integer.parseInt(result));
                break;
            case "GetUser":
                getUserProfileActivity(result);
                break;
            case "EditUser":
                editUserActivity(Integer.parseInt(result));
                break;
            case "ChangeUserPassword":
                changeUserPasswordActivity(Integer.parseInt(result));
                break;
            case "GetReceipts":
                getReceiptsActivity(result);
                break;
            case "GetReceipt":
                getReceiptActivity(result);
                break;
            case "UpdateReceipt":
                updateReceiptActivity(Integer.parseInt(result));
                break;
            case "DeleteReceipt":
                deleteReceiptActivity(Integer.parseInt(result));
                break;
            case "GetProductsOnReceipt":
                getProductsOnReceiptActivity(result);
                break;
            case "GetMaterialsOnReceipt":
                getMaterialsOnReceiptActivity(result);
                break;
            case "AddReceiptPicture":
                addReceiptPictureActivity(Integer.parseInt(result));
                break;
            case "AddReceiptCode":
                addReceiptCodeActivity(Integer.parseInt(result));
                break;
            case "GetProductCategories":
                getProductCategoriesActivity(result);
                break;
            case "GetUserMaterials":
                getUserMaterialsActivity(result);
                break;
            case "GetProductsShopWindow":
                getProductsShopWindowActivity(result);
                break;
            case "GetProductShopWindow":
                getProductShopWindowActivity(result);
                break;
            case "GetProductPicturesShopWindow":
                getProductPicturesShopWindowActivity(result);
                break;
        }
    }
}

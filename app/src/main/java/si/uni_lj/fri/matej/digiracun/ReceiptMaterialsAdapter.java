package si.uni_lj.fri.matej.digiracun;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ReceiptMaterialsAdapter extends RecyclerView.Adapter<ReceiptMaterialsAdapter.ViewHolder> {
    private List<JSONObject> receiptMaterials;
    private LayoutInflater inflater;
    private ItemClickListener clickListener;

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        ImageView iv_material;
        TextView tv_productName, tv_description;

        public ViewHolder(View view) {
            super(view);
            iv_material = view.findViewById(R.id.IV_receiptMaterialItem_material);
            tv_productName = view.findViewById(R.id.TV_receiptMaterialItem_productName);
            tv_description = view.findViewById(R.id.TV_receiptMaterialItem_description);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public ReceiptMaterialsAdapter(Context context, List<JSONObject> receiptMaterials) {
        this.inflater = LayoutInflater.from(context);
        this.receiptMaterials = receiptMaterials;
    }

    // inflates the row layout from xml when needed
    @Override
    public ReceiptMaterialsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = inflater.inflate(R.layout.item_receipt_material, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JSONObject material = receiptMaterials.get(position);

        try {
            if (material.getBoolean("picture")) {
                byte[] decoded = Base64.decode(material.getString("material"), Base64.DEFAULT);
                final Bitmap picture = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
                holder.iv_material.setImageBitmap(picture);
            } else {
                holder.iv_material.setImageResource(R.drawable.ic_file_pdf);
            }

            holder.tv_productName.setText(material.getString("productName"));

            String description = material.getString("description");

            if (description.length() > 80) {
                description = description.substring(0, 79);
                description += "...";
            }

            holder.tv_description.setText(description);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return receiptMaterials.size();
    }

    // convenience method for getting data at click position
    JSONObject getItem(int id) {
        return receiptMaterials.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

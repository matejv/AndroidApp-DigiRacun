package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static si.uni_lj.fri.matej.digiracun.BaseClass.resizePicture;
import static si.uni_lj.fri.matej.digiracun.BaseClass.toDateTransfer;

public class AddReceiptPictureActivity extends AppCompatActivity {
    private static String username, password;
    EditText et_shop, et_receiptNumber, et_dateOfPurchase, et_dateOfGuarantee;
    ImageView iv_receiptPicture;
    Button b_camera, b_gallery;
    private static final int REQUEST_IMAGE_CAMERA = 1;
    private static final int REQUEST_IMAGE_GALLERY = 2;
    private static final int PICTURE_MAX_WIDTH = 1000, PICTURE_MAX_HEIGHT = 1000;
    private File pictureFile;
    private Bitmap pictureBitmap;
    private boolean pictureSet; // if user added pictureFile

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receipt_picture);

        et_shop = findViewById(R.id.ET_addReceiptPicture_shop);
        et_receiptNumber = findViewById(R.id.ET_addReceiptPicture_receiptNumber);
        et_dateOfPurchase = findViewById(R.id.ET_addReceiptPicture_dateOfPurchase);
        et_dateOfGuarantee = findViewById(R.id.ET_addReceiptPicture_dateOfGuarantee);
        iv_receiptPicture = findViewById(R.id.IV_addReceiptPicture_receiptPicture);
        b_camera = findViewById(R.id.B_addReceiptPicture_camera);
        b_gallery = findViewById(R.id.B_addReceiptPicture_gallery);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");

        pictureFile = null;
        pictureSet = false;

        // automatically set date of purchase to today's date
        SimpleDateFormat sdf = new SimpleDateFormat("dd. MM. yyyy");
        et_dateOfPurchase.setText(sdf.format(new Date()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_receipt_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_userMaterials:
                intent = new Intent(this, UserMaterialsActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_shopWindow:
                intent = new Intent(this, ShopWindowActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_userProfile:
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAMERA && resultCode == RESULT_OK) {
            pictureBitmap = resizePicture(BitmapFactory.decodeFile(this.pictureFile.getAbsolutePath()), PICTURE_MAX_WIDTH, PICTURE_MAX_HEIGHT);

            iv_receiptPicture.setImageBitmap(pictureBitmap);
            pictureSet = true;
            resetColorsImageSet();
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            Uri selectedPicUri = data.getData();
            InputStream inputStream = null;

            try {
                inputStream = getContentResolver().openInputStream(selectedPicUri);
                pictureBitmap = resizePicture(BitmapFactory.decodeStream(inputStream), PICTURE_MAX_WIDTH, PICTURE_MAX_HEIGHT);
                inputStream.close();

                // create temporary file of selected picture
                File outputDir = getExternalCacheDir();
                pictureFile = File.createTempFile("receiptPicture", ".jpg", outputDir);
                FileOutputStream tempPicture = new FileOutputStream(pictureFile.getAbsoluteFile());
                pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, tempPicture);
                tempPicture.flush();
                tempPicture.close();

                iv_receiptPicture.setImageBitmap(pictureBitmap);
                pictureSet = true;
                resetColorsImageSet();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void B_addReceiptPicture_camera_onClick(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            try {
                File outputDir = view.getContext().getExternalCacheDir();
                pictureFile = File.createTempFile("receiptPicture", ".jpg", outputDir);
            } catch (IOException e) {
                e.printStackTrace();
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(pictureFile));
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, REQUEST_IMAGE_CAMERA);
        }
    }

    public void B_addReceiptPicture_gallery_onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        if (intent.resolveActivity(getPackageManager()) != null) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
        }
    }

    private void resetColorsImageSet() {
        b_camera.getBackground().clearColorFilter();
        b_gallery.getBackground().clearColorFilter();
    }

    private void resetColors() {
        resetColorsImageSet();
        et_shop.getBackground().clearColorFilter();
        et_dateOfPurchase.getBackground().clearColorFilter();
    }

    private boolean checkInput() {
        resetColors();
        boolean inputsOk = true;

        if (et_shop.getText().length() == 0) {
            et_shop.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        if (et_dateOfPurchase.getText().length() == 0) {
            et_dateOfPurchase.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        if (!pictureSet) {
            b_camera.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            b_gallery.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        return inputsOk;
    }

    private JSONObject createJsonObject() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("shop", et_shop.getText().toString());
            jsonObject.put("dateOfPurchase", toDateTransfer(et_dateOfPurchase.getText().toString()));
            jsonObject.put("receiptPicFileName", pictureFile.getName());

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            jsonObject.put("receiptPicture", Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));

            if (et_receiptNumber.getText().length() > 0)
                jsonObject.put("receiptNumber", et_receiptNumber.getText().toString());

            if (et_dateOfGuarantee.getText().length() > 0)
                jsonObject.put("dateOfGuarantee", toDateTransfer(et_dateOfGuarantee.getText().toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public void B_addReceiptPicture_addByPicture_onClick(View view) {
        if (checkInput()) {
            ServiceCaller serviceCaller = new ServiceCaller(this, view, "AddReceiptPicture", username, password);
            serviceCaller.setBody(createJsonObject().toString());
            serviceCaller.execute("post", "true", "true");
        }
    }
}

package si.uni_lj.fri.matej.digiracun;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class BaseClass {
    /**
     * DELETE CACHE
     */
    static void deleteCache(Context context) {
        try {
            File dir = context.getExternalCacheDir();

            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();

            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));

                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    /**
     * DATE
     */
    // converts input date in a string suitable for transfer
    static String toDateTransfer(String inputDate) {
        inputDate = inputDate.replace(" ", "");
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date outputDate = null;

        try {
            outputDate = inputFormat.parse(inputDate); // converts argument string
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // if conversion is not successful return argument string
        if (outputDate == null)
            return inputDate;

        // date format string suitable for transfer
        SimpleDateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

        return outputFormat.format(outputDate);
    }

    // converts the date in a string suitable for displaying
    static String toDateDisplay(String dateTime) {
        // date format in argument string
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
        Date date = null;

        try {
            date = format.parse(dateTime); // converts argument string
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // if conversion is not successful return argument string
        if (date == null)
            return dateTime;

        // date format string suitable for displaying
        SimpleDateFormat sdf = new SimpleDateFormat("dd. MM. yyyy");

        return sdf.format(date);
    }

    /**
     * RESIZE BITMAP PICTURE
     */
    static Bitmap resizePicture(Bitmap originalPicture, int maxWidth, int maxHeight) {
        double newRatio;
        int newWidth = 0;
        int newHeight = 0;

        if (originalPicture.getWidth() < maxWidth && originalPicture.getHeight() < maxHeight)
            return originalPicture;

        if (originalPicture.getWidth() > originalPicture.getHeight()) {
            newRatio = (double) maxWidth / originalPicture.getWidth();
            newWidth = maxWidth;
            double tmpHeight = originalPicture.getHeight() * newRatio;
            newHeight = (int) tmpHeight;
        } else {
            newRatio = (double) maxHeight / originalPicture.getHeight();
            newHeight = maxHeight;
            double tmpWidth = originalPicture.getWidth() * newRatio;
            newWidth = (int) tmpWidth;
        }

        return Bitmap.createScaledBitmap(originalPicture, newWidth, newHeight, false);
    }
}

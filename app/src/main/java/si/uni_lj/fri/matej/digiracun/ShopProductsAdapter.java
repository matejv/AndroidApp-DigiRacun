package si.uni_lj.fri.matej.digiracun;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ShopProductsAdapter extends RecyclerView.Adapter<ShopProductsAdapter.ViewHolder> {
    private List<JSONObject> shopProducts;
    private LayoutInflater inflater;
    private ItemClickListener clickListener;

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        ImageView iv_coverPicture;
        TextView tv_productName, tv_price, tv_shop;

        public ViewHolder(View view) {
            super(view);
            iv_coverPicture = view.findViewById(R.id.IV_shopProductItem_coverPicture);
            tv_productName = view.findViewById(R.id.TV_shopProductItem_productName);
            tv_price = view.findViewById(R.id.TV_shopProductItem_price);
            tv_shop = view.findViewById(R.id.TV_shopProductItem_shop);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public ShopProductsAdapter(Context context, List<JSONObject> shopProducts) {
        this.inflater = LayoutInflater.from(context);
        this.shopProducts = shopProducts;
    }

    // inflates the row layout from xml when needed
    @Override
    public ShopProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = inflater.inflate(R.layout.item_shop_product, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JSONObject product = shopProducts.get(position);

        try {
            byte[] decoded = Base64.decode(product.getString("coverPicture"), Base64.DEFAULT);
            final Bitmap picture = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);

            holder.iv_coverPicture.setImageBitmap(picture);
            holder.tv_productName.setText(product.getString("productName"));
            holder.tv_price.setText(String.format("%.2f €", product.getDouble("price")));
            holder.tv_shop.setText(product.getString("shop"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return shopProducts.size();
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        try {
            return shopProducts.get(id).getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

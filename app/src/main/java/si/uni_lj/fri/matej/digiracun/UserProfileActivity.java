package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

public class UserProfileActivity extends AppCompatActivity {
    private static String username, password;
    EditText et_username, et_name, et_surname, et_email;
    EditText et_passwordNow, et_passwordNew1, et_passwordNew2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        et_username = findViewById(R.id.ET_userProfile_username);
        et_name = findViewById(R.id.ET_userProfile_name);
        et_surname = findViewById(R.id.ET_userProfile_surname);
        et_email = findViewById(R.id.ET_userProfile_email);
        et_passwordNow = findViewById(R.id.ET_userProfile_passwordNow);
        et_passwordNew1 = findViewById(R.id.ET_userProfile_passwordNew1);
        et_passwordNew2 = findViewById(R.id.ET_userProfile_passwordNew2);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");

        ServiceCaller serviceCaller = new ServiceCaller(this, "GetUser", username, password);
        serviceCaller.execute("get", "true");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // change user profile
    private void resetColorsProfile() {
        et_email.getBackground().clearColorFilter();
    }

    private boolean checkInputProfile() {
        resetColorsProfile();
        boolean inputsOk = true;
        Pattern patternEmail = Pattern.compile("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");

        if (!patternEmail.matcher(et_email.getText()).matches()) {
            et_email.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        return inputsOk;
    }

    private JSONObject createJsonObject() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("username", username);
            jsonObject.put("email", et_email.getText().toString());

            if (et_name.getText().length() > 0)
                jsonObject.put("name", et_name.getText().toString());

            if (et_surname.getText().length() > 0)
                jsonObject.put("surname", et_surname.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public void B_userProfile_changeProfile_onClick(View view) {
        if (checkInputProfile()) {
            ServiceCaller serviceCaller = new ServiceCaller(this, "EditUser", username, password);
            serviceCaller.setBody(createJsonObject().toString());
            serviceCaller.execute("post", "true", "true");
        }
    }

    // change password
    private void resetColorsPassword() {
        et_passwordNow.getBackground().clearColorFilter();
        et_passwordNew1.getBackground().clearColorFilter();
        et_passwordNew2.getBackground().clearColorFilter();
    }

    private boolean checkInputPassword() {
        resetColorsPassword();
        boolean inputsOk = true;

        if (!et_passwordNow.getText().toString().equals(password)) {
            et_passwordNow.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        if (et_passwordNew1.getText().length() < 8) {
            et_passwordNew1.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        if (!et_passwordNew2.getText().toString().equals(et_passwordNew1.getText().toString())) {
            et_passwordNew2.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        return inputsOk;
    }

    public void B_userProfile_changePassword_onClick(View view) {
        if (checkInputPassword()) {
            ServiceCaller serviceCaller = new ServiceCaller(this, "ChangeUserPassword", username, password);
            serviceCaller.setBody(et_passwordNew1.getText().toString());
            serviceCaller.execute("post", "true", "true");
        }
    }
}

package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {
    EditText et_username, et_name, et_surname, et_email, et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_username = findViewById(R.id.ET_register_username);
        et_name = findViewById(R.id.ET_register_name);
        et_surname = findViewById(R.id.ET_register_surname);
        et_email = findViewById(R.id.ET_register_email);
        et_password = findViewById(R.id.ET_register_password);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        et_username.setText(thisIntent.getStringExtra("username"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_shopWindow:
                Intent intent = new Intent(this, ShopWindowActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void resetColors() {
        et_username.getBackground().clearColorFilter();
        et_email.getBackground().clearColorFilter();
        et_password.getBackground().clearColorFilter();
    }

    private boolean checkInput() {
        resetColors();
        boolean inputsOk = true;
        Pattern patternUsername = Pattern.compile("^[A-Za-z0-9]{5,20}$");
        Pattern patternEmail = Pattern.compile("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");

        if (!patternUsername.matcher(et_username.getText()).matches()) {
            et_username.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        if (!patternEmail.matcher(et_email.getText()).matches()) {
            et_email.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        if (et_password.getText().length() < 8) {
            et_password.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            inputsOk = false;
        }

        return inputsOk;
    }

    private JSONObject createJsonObject() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("username", et_username.getText().toString());
            jsonObject.put("email", et_email.getText().toString());
            jsonObject.put("password", et_password.getText().toString());

            if (et_name.getText().length() > 0)
                jsonObject.put("name", et_name.getText().toString());

            if (et_surname.getText().length() > 0)
                jsonObject.put("surname", et_surname.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public void B_register_register_onClick(View view) {
        if (checkInput()) {
            ServiceCaller serviceCaller = new ServiceCaller(this, view, "Register", et_username.getText().toString(), et_password.getText().toString());
            serviceCaller.setBody(createJsonObject().toString());
            serviceCaller.execute("post", "false", "true");
        }
    }
}

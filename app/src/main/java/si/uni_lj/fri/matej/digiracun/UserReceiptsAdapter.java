package si.uni_lj.fri.matej.digiracun;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static si.uni_lj.fri.matej.digiracun.BaseClass.toDateDisplay;

public class UserReceiptsAdapter extends RecyclerView.Adapter<UserReceiptsAdapter.ViewHolder> {
    private List<JSONObject> userReceipts;
    private LayoutInflater inflater;
    private ItemClickListener clickListener;

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        TextView tv_receiptNumber, tv_shop, tv_dateOfPurchase;

        public ViewHolder(View view) {
            super(view);
            tv_receiptNumber = view.findViewById(R.id.TV_userReceiptItem_receiptNumber);
            tv_shop = view.findViewById(R.id.TV_userReceiptItem_shop);
            tv_dateOfPurchase = view.findViewById(R.id.TV_userReceiptItem_dateOfPurchase);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public UserReceiptsAdapter(Context context, List<JSONObject> userReceipts) {
        this.inflater = LayoutInflater.from(context);
        this.userReceipts = userReceipts;
    }

    // inflates the row layout from xml when needed
    @Override
    public UserReceiptsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = inflater.inflate(R.layout.item_user_receipt, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JSONObject receipt = userReceipts.get(position);

        try {
            holder.tv_receiptNumber.setText(receipt.getString("receiptNumber"));
            holder.tv_shop.setText(receipt.getString("shop"));
            holder.tv_dateOfPurchase.setText(toDateDisplay(receipt.getString("dateOfPurchase")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return userReceipts.size();
    }

    // convenience method for getting data at click position
    JSONObject getItem(int id) {
        return userReceipts.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

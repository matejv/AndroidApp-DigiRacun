package si.uni_lj.fri.matej.digiracun;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static si.uni_lj.fri.matej.digiracun.BaseClass.toDateTransfer;

public class EditReceiptActivity extends AppCompatActivity {
    private static String username, password, receiptId;
    private static boolean code; // if true layout is adjusted for receipt with code

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapterPicture mSectionsPagerAdapterPicture;
    private SectionsPagerAdapterCode mSectionsPagerAdapterCode;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_receipt);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TabLayout tabLayout = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.container);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");
        receiptId = thisIntent.getStringExtra("receiptId");

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        // Set up the ViewPager with the sections adapter.
        if (thisIntent.getStringExtra("code").length() == 0) {
            code = false;

            // adjust tabs
            tabLayout.getTabAt(0).setText(R.string.TI_editReceipt_fragment0);
            tabLayout.getTabAt(1).setText(R.string.TI_editReceipt_fragment1);
            tabLayout.removeTabAt(2);

            mSectionsPagerAdapterPicture = new SectionsPagerAdapterPicture(getSupportFragmentManager());
            mViewPager.setAdapter(mSectionsPagerAdapterPicture);
        } else {
            code = true;

            // adjust tabs
            tabLayout.getTabAt(0).setText(R.string.TI_editReceipt_fragment0);
            tabLayout.getTabAt(1).setText(R.string.TI_editReceipt_fragment2);
            tabLayout.getTabAt(2).setText(R.string.TI_editReceipt_fragment3);

            mSectionsPagerAdapterCode = new SectionsPagerAdapterCode(getSupportFragmentManager());
            mViewPager.setAdapter(mSectionsPagerAdapterCode);
        }

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        ServiceCaller serviceCaller = new ServiceCaller(this, mViewPager,
                String.format("GetReceipt/%s", receiptId), username, password);
        serviceCaller.execute("get", "true");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_receipt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_userMaterials:
                intent = new Intent(this, UserMaterialsActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_shopWindow:
                intent = new Intent(this, ShopWindowActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_userProfile:
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment 0 containing a simple view.
     */
    public static class EditReceiptFragment0 extends Fragment {
        EditText et_shop, et_receiptNumber, et_dateOfPurchase, et_dateOfGuarantee;
        TextView tv_dateOfGuarantee;
        Button b_save, b_deleteReceipt;

        public EditReceiptFragment0() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment0_edit_receipt, container, false);
            et_shop = rootView.findViewById(R.id.ET_editReceipt_shop);
            et_receiptNumber = rootView.findViewById(R.id.ET_editReceipt_receiptNumber);
            et_dateOfPurchase = rootView.findViewById(R.id.ET_editReceipt_dateOfPurchase);
            et_dateOfGuarantee = rootView.findViewById(R.id.ET_editReceipt_dateOfGuarantee);
            tv_dateOfGuarantee = rootView.findViewById(R.id.TV_editReceipt_dateOfGuarantee);
            b_save = rootView.findViewById(R.id.B_editReceipt_save);
            b_deleteReceipt = rootView.findViewById(R.id.B_editReceipt_deleteReceipt);

            // receipt with code has adjusted layout
            if (code) {
                et_shop.setEnabled(false);
                et_receiptNumber.setEnabled(false);
                et_dateOfPurchase.setEnabled(false);
                tv_dateOfGuarantee.setVisibility(View.GONE);
                et_dateOfGuarantee.setVisibility(View.GONE);
                b_save.setEnabled(false);
                b_save.setVisibility(View.GONE);
            }

            b_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkInput()) {
                        ServiceCaller serviceCaller = new ServiceCaller(getActivity(), "UpdateReceipt", username, password);
                        serviceCaller.setBody(createJsonObject().toString());
                        serviceCaller.execute("post", "true", "true");
                    }
                }
            });

            b_deleteReceipt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(rootView.getContext());
                    dialog.setTitle(R.string.editReceipt_deleteReceipt_title);
                    dialog.setMessage(R.string.editReceipt_deleteReceipt_message);
                    dialog.setNegativeButton(R.string.editReceipt_deleteReceipt_negative, null);
                    dialog.setPositiveButton(R.string.editReceipt_deleteReceipt_positive, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ServiceCaller serviceCaller = new ServiceCaller(getActivity(), rootView, "DeleteReceipt", username, password);
                            serviceCaller.setBody(receiptId);
                            serviceCaller.execute("post", "true", "true");
                        }
                    });
                    dialog.show();
                }
            });

            return rootView;
        }

        private void resetColors() {
            et_shop.getBackground().clearColorFilter();
            et_dateOfPurchase.getBackground().clearColorFilter();
        }

        private boolean checkInput() {
            resetColors();
            boolean inputsOk = true;

            if (et_shop.getText().length() == 0) {
                et_shop.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                inputsOk = false;
            }

            if (et_dateOfPurchase.getText().length() == 0) {
                et_dateOfPurchase.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                inputsOk = false;
            }

            return inputsOk;
        }

        private JSONObject createJsonObject() {
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("id", receiptId);
                jsonObject.put("shop", et_shop.getText().toString());
                jsonObject.put("dateOfPurchase", toDateTransfer(et_dateOfPurchase.getText().toString()));

                if (et_receiptNumber.getText().length() > 0)
                    jsonObject.put("receiptNumber", et_receiptNumber.getText().toString());

                if (et_dateOfGuarantee.getText().length() > 0)
                    jsonObject.put("dateOfGuarantee", toDateTransfer(et_dateOfGuarantee.getText().toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject;
        }
    }

    /**
     * A placeholder fragment 1 containing a simple view for receipt picture.
     */
    public static class EditReceiptFragment1 extends Fragment {
        public EditReceiptFragment1() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment1_edit_receipt, container, false);
            return rootView;
        }
    }

    /**
     * A placeholder fragment 2 containing a simple view for receiptProducts on receipt.
     */
    public static class EditReceiptFragment2 extends Fragment {
        public static ArrayList<JSONObject> receiptProducts;
        static ReceiptProductsAdapter adapter;
        RecyclerView rv_products;

        public EditReceiptFragment2() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment2_edit_receipt, container, false);
            receiptProducts = new ArrayList<JSONObject>();

            ServiceCaller serviceCaller = new ServiceCaller(getActivity(),
                    String.format("GetProductsOnReceipt/%s", receiptId), username, password);
            serviceCaller.execute("get", "true");

            // wait for a list receiptProducts from web service up to 500 milliseconds
            try {
                serviceCaller.get(500, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            rv_products = rootView.findViewById(R.id.RV_editReceipt_products);
            LinearLayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
            rv_products.setLayoutManager(layoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_products.getContext(), layoutManager.getOrientation());
            rv_products.addItemDecoration(dividerItemDecoration);
            adapter = new ReceiptProductsAdapter(rootView.getContext(), receiptProducts);
            rv_products.setAdapter(adapter);

            return rootView;
        }

        public static void refreshData() {
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * A placeholder fragment 3 containing a simple view for receiptMaterials of receiptProducts on receipt.
     */
    public static class EditReceiptFragment3 extends Fragment implements ReceiptMaterialsAdapter.ItemClickListener {
        public static ArrayList<JSONObject> receiptMaterials;
        static ReceiptMaterialsAdapter adapter;
        RecyclerView rv_materials;

        public EditReceiptFragment3() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment3_edit_receipt, container, false);
            receiptMaterials = new ArrayList<JSONObject>();

            ServiceCaller serviceCaller = new ServiceCaller(getActivity(),
                    String.format("GetMaterialsOnReceipt/%s", receiptId), username, password);
            serviceCaller.execute("get", "true");

            // wait for a list receiptMaterials from web service up to 500 milliseconds
            try {
                serviceCaller.get(500, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            rv_materials = rootView.findViewById(R.id.RV_editReceipt_materials);
            LinearLayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
            rv_materials.setLayoutManager(layoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_materials.getContext(), layoutManager.getOrientation());
            rv_materials.addItemDecoration(dividerItemDecoration);
            adapter = new ReceiptMaterialsAdapter(rootView.getContext(), receiptMaterials);
            adapter.setClickListener(this);
            rv_materials.setAdapter(adapter);

            return rootView;
        }

        public static void refreshData() {
            Collections.sort(receiptMaterials, new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject o1, JSONObject o2) {
                    String pn1, pn2;

                    try {
                        pn1 = o1.getString("productName");
                        pn2 = o2.getString("productName");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return 0;
                    }

                    return pn1.compareToIgnoreCase(pn2);
                }
            });

            adapter.notifyDataSetChanged();
        }

        @Override
        public void onItemClick(View view, int position) {
            JSONObject jsonObject = adapter.getItem(position);

            try {
                byte[] decoded = Base64.decode(jsonObject.getString("material"), Base64.DEFAULT);
                String fileName = jsonObject.getString("fileName");

                if (jsonObject.getBoolean("picture")) {
                    if (fileName.length() == 0) {
                        fileName = "tempMaterialPicture.jpg";
                    }

                    File outputDir = new File(view.getContext().getExternalCacheDir(), fileName); // context being the Activity pointer
                    FileOutputStream tempFile = new FileOutputStream(outputDir.getAbsoluteFile());

                    final Bitmap picture = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
                    picture.compress(Bitmap.CompressFormat.JPEG, 100, tempFile);
                    tempFile.flush();
                    tempFile.close();

                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setDataAndType(Uri.parse("file:" + outputDir.getAbsolutePath()), "image/*");
                    startActivity(intent);
                } else {
                    if (fileName.length() == 0) {
                        fileName = "tempMaterialFile.pdf";
                    }

                    File outputDir = new File(view.getContext().getExternalCacheDir(), fileName); // context being the Activity pointer
                    FileOutputStream tempFile = new FileOutputStream(outputDir.getAbsoluteFile());

                    tempFile.write(decoded);
                    tempFile.flush();
                    tempFile.close();

                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setDataAndType(Uri.parse("file:" + outputDir.getAbsolutePath()), "application/pdf");
                    startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     * Receipt with picture
     */
    public class SectionsPagerAdapterPicture extends FragmentPagerAdapter {

        public SectionsPagerAdapterPicture(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new EditReceiptFragment0();
                case 1:
                    return new EditReceiptFragment1();
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     * Receipt with code
     */
    public class SectionsPagerAdapterCode extends FragmentPagerAdapter {

        public SectionsPagerAdapterCode(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new EditReceiptFragment0();
                case 1:
                    return new EditReceiptFragment2();
                case 2:
                    return new EditReceiptFragment3();
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}

package si.uni_lj.fri.matej.digiracun;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ShopWindowProductActivity extends AppCompatActivity {
    private static String username, password;
    private static String productId;
    final private static String SHARE_MSG = "Digi Račun -> %s: https://digiracun.azurewebsites.net/ShopWindowProduct.aspx?id=%s";
    FloatingActionButton fab_share;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_window_product);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TabLayout tabLayout = findViewById(R.id.tabs);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        fab_share = findViewById(R.id.FAB_shopWindowProduct_share);

        // get intent data for this activity
        Intent thisIntent = getIntent();
        username = thisIntent.getStringExtra("username");
        password = thisIntent.getStringExtra("password");
        productId = thisIntent.getStringExtra("productId");

        ServiceCaller getProduct = new ServiceCaller(this,
                String.format("GetProductShopWindow/%s", productId));
        getProduct.execute("get", "false");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (username != null && password != null) {
            getMenuInflater().inflate(R.menu.menu_shop_window_product_logged, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_shop_window_product, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableSwitchStatement
        switch (id) {
            case R.id.action_share:
                fab_share.performClick();
                return true;
            case R.id.action_login:
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_home:
                intent = new Intent(this, UserActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_userProfile:
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                AppPreferences.clearLoginData(this);
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("username", username);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_exit:
                finishAffinity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void FAB_shopWindowProduct_share_onClick(View view) {
        String productName = ((TextView) findViewById(R.id.TV_shopWindowProduct_productName)).getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, String.format(SHARE_MSG, productName, productId));

        startActivity(Intent.createChooser(intent, getString(R.string.shopWindowProduct_share_title)));
    }

    /**
     * A placeholder fragment 0 containing a simple view for product data.
     */
    public static class ShopWindowProductFragment0 extends Fragment {
        public static ArrayList<Bitmap> shopProductPictures;
        private static int picturePosition;
        ImageView iv_pictures;
        ImageButton ib_before, ib_next;
        TextView tv_description, tv_techDetails;

        public ShopWindowProductFragment0() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment0_shop_window_product, container, false);
            shopProductPictures = new ArrayList<Bitmap>();
            picturePosition = 0;
            iv_pictures = rootView.findViewById(R.id.IV_shopWindowProduct_pictures);
            ib_before = rootView.findViewById(R.id.IB_shopWindowProduct_before);
            ib_next = rootView.findViewById(R.id.IB_shopWindowProduct_next);
            tv_description = rootView.findViewById(R.id.TV_shopWindowProduct_description);
            tv_techDetails = rootView.findViewById(R.id.TV_shopWindowProduct_techDetails_val);

            ib_before.setEnabled(false);
            ib_next.setEnabled(false);
            tv_description.setSingleLine(false);
            tv_techDetails.setSingleLine(false);

            ib_before.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (picturePosition > 0) {
                        iv_pictures.setImageBitmap(shopProductPictures.get(--picturePosition));
                        ib_next.setEnabled(true);
                    }

                    if (picturePosition == 0) {
                        ib_before.setEnabled(false);
                    }
                }
            });

            ib_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (shopProductPictures.size() - 1 > picturePosition) {
                        iv_pictures.setImageBitmap(shopProductPictures.get(++picturePosition));
                        ib_before.setEnabled(true);
                    }

                    if (picturePosition == shopProductPictures.size() - 1) {
                        ib_next.setEnabled(false);
                    }
                }
            });

            ServiceCaller getPictures = new ServiceCaller(getActivity(),
                    String.format("GetProductPicturesShopWindow/%s", productId));
            getPictures.execute("get", "false");

            return rootView;
        }
    }

    /**
     * A placeholder fragment 1 containing a simple view for shop data.
     */
    public static class ShopWindowProductFragment1 extends Fragment {
        TextView tv_address;

        public ShopWindowProductFragment1() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment1_shop_window_product, container, false);
            tv_address = rootView.findViewById(R.id.TV_shopWindowProduct_address_val);

            tv_address.setSingleLine(false);

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new ShopWindowProductFragment0();
                case 1:
                    return new ShopWindowProductFragment1();
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }
}
